<?php declare(strict_types=1);
namespace AnivaBay\Api\Http;

use Psr\Http\Message\ServerRequestInterface;
use AnivaBay\Api\Http\Router\Exception\MethodNotAllowedException;
use AnivaBay\Api\Http\Router\Exception\RouteNotFoundException;
use AnivaBay\Api\Http\Router\MatchedRoute;
use AnivaBay\Api\Http\Router\Route;
use FastRoute\DataGenerator\GroupCountBased as DataGenerator;
use FastRoute\Dispatcher\GroupCountBased as Dispatcher;
use FastRoute\RouteCollector as FastRoute;
use FastRoute\RouteParser\Std as RouteParser;

class Router
{
    /**
     * @var Dispatcher
     */
    protected $dispatcher;

    /**
     * Router constructor.
     * @param Route ...$routes
     */
    public function __construct(Route ...$routes)
    {
        $router = new FastRoute(new RouteParser(), new DataGenerator());
        foreach ($routes as $route) {
            $router->addRoute(strtoupper($route->getMethod()), $route->getPath(), $route->getPayload());
        }
        $this->dispatcher = new Dispatcher($router->getData());
    }

    /**
     * @param ServerRequestInterface $request
     * @return MatchedRoute
     * @throws MethodNotAllowedException
     * @throws RouteNotFoundException
     */
    public function dispatch(ServerRequestInterface $request): MatchedRoute
    {
        $routeInfo = $this->dispatcher->dispatch(
            strtoupper($request->getMethod()),
            $request->getUri()->getPath()
        );

        switch ($routeInfo[0]) {
            case Dispatcher::NOT_FOUND:
                throw new RouteNotFoundException();

            case Dispatcher::METHOD_NOT_ALLOWED:
                throw new MethodNotAllowedException($routeInfo[1]);

            default:
                return new MatchedRoute($request->getUri()->getPath(), $routeInfo[1], $routeInfo[2]);
        }
    }
}
