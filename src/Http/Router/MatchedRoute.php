<?php declare(strict_types=1);

namespace AnivaBay\Api\Http\Router;

class MatchedRoute
{
    /**
     * @var string
     */
    protected $method;
    /**
     * @var mixed
     */
    protected $payload;

    /**
     * @var array
     */
    protected $args;
    /**
     * MatchedRoute constructor.
     * @param $method
     * @param mixed $payload
     * @param array $args
     */
    public function __construct($method, $payload, array $args)
    {
        $this->method = $method;
        $this->payload = $payload;
        $this->args = $args;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }
    /**
     * @return mixed
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @return array
     */
    public function getArgs(): array
    {
        return $this->args;
    }
}
