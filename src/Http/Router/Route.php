<?php declare(strict_types=1);

namespace AnivaBay\Api\Http\Router;

class Route
{
    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $method;

    /**
     * @var mixed
     */
    protected $payload;

    /**
     * Route constructor.
     * @param string $path
     * @param string $method
     * @param mixed $payload
     */
    public function __construct(string $path, string $method, $payload)
    {
        $this->path = $path;
        $this->method = $method;
        $this->payload = $payload;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return string[]
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string[] $method
     */
    public function setMethod(array $method): void
    {
        $this->method = $method;
    }

    /**
     * @return mixed
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @param mixed $payload
     */
    public function setPayload($payload): void
    {
        $this->payload = $payload;
    }
}
