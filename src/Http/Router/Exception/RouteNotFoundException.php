<?php declare(strict_types=1);

namespace AnivaBay\Api\Http\Router\Exception;

class RouteNotFoundException extends RouterException
{

}
