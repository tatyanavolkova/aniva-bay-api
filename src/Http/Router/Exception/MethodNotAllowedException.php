<?php declare(strict_types=1);

namespace AnivaBay\Api\Http\Router\Exception;

class MethodNotAllowedException extends RouterException
{
    /**
     * @var string[]
     */
    protected $allowedMethods;

    /**
     * MethodNotAllowedException constructor.
     * @param string[] $allowedMethods
     */
    public function __construct(array $allowedMethods)
    {
        $this->allowedMethods = $allowedMethods;
    }

    /**
     * @return string[]
     */
    public function getAllowedMethods(): array
    {
        return $this->allowedMethods;
    }
}
