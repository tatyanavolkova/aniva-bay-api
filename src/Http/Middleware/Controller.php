<?php declare(strict_types=1);
namespace AnivaBay\Api\Http\Middleware;

use Acelot\Resolver\ResolverInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use AnivaBay\Api\Http\Router\MatchedRoute;

class Controller implements MiddlewareInterface
{
    /**
     * @var ResolverInterface
     */
    private $resolver;

    /**
     * Controller constructor.
     * @param ResolverInterface $resolver
     */
    public function __construct(ResolverInterface $resolver)
    {
        $this->resolver = $resolver;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        /** @var MatchedRoute $matchedRoute */
        $matchedRoute = $request->getAttribute('matchedRoute');
        $controller = $this->resolver->resolve('\\AnivaBay\\Api\\Controller\\' . $matchedRoute->getPayload()['operationId']);
        return $controller->call($request);
    }
}