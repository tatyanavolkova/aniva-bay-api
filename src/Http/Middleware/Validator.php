<?php
namespace AnivaBay\Api\Http\Middleware;

use JsonSchema\Constraints\Factory;
use JsonSchema\SchemaStorage;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use JsonSchema\Validator as JsonValidator;
use Psr\Log\LoggerInterface;
use AnivaBay\Api\Helpers\ArrayHelper;
use AnivaBay\Api\Infrastructure\Config;

class Validator implements MiddlewareInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Validator constructor.
     * @param Config $config
     * @param LoggerInterface $logger
     */
    public function __construct(Config $config, LoggerInterface $logger)
    {
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $matchedRoute = $request->getAttribute('matchedRoute');

        $body = $request->getParsedBody();
        if (!is_object($body)) {
            return $handler->handle($request);
        }

        $schema['components'] = $this->config->get('api.components');
        $schema = ArrayHelper::arrayToObject($schema);

        $schemaStorage = new SchemaStorage();
        $schemaStorage->addSchema('file://mySchema', $schema);

        $validator = new JsonValidator( new Factory($schemaStorage));
        $validator->validate($body, $schema);

        if (!$validator->isValid()) {
            //$this->logger->
            return new JsonResponse([
                'message' => 'Invalid request',
                'errors' => $validator->getErrors()
            ], HttpStatus::STATUS_UNPROCESSABLE_ENTITY);
        }
        return $handler->handle($request->withAttribute('matchedRoute', $matchedRoute));
    }
}