<?php

namespace AnivaBay\Api\Http\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;

class JsonParser implements MiddlewareInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $parsedBody = json_decode($request->getBody());
        if (!is_object($parsedBody)) {
            return $handler->handle($request);
        }
        if ($parsedBody) {
            return $handler->handle($request->withParsedBody($parsedBody));
        } else {
            return new JsonResponse(['message' => 'Bad request'], 400);
        }
    }
}