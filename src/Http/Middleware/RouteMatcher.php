<?php declare(strict_types=1);
namespace AnivaBay\Api\Http\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use AnivaBay\Api\Http\Router;
use AnivaBay\Api\Http\Router\Route;
use AnivaBay\Api\Http\Router\Exception\RouteNotFoundException;
use AnivaBay\Api\Http\Router\Exception\MethodNotAllowedException;
use AnivaBay\Api\Infrastructure\Config;
use Zend\Diactoros\Response\JsonResponse;

class RouteMatcher implements MiddlewareInterface
{
    /**
     * @var Router
     */
    private $router;

    /**
     * RouteMatcher constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $routes = [];
        foreach ($config->get('api.paths') as $path => $methods) {
            foreach ($methods as $method => $data) {
                $routes[] = new Route($path, $method, $data);
            }
        }

        $this->router = new Router(...$routes);
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            $matchedRoute = $this->router->dispatch($request);
            return $handler->handle($request->withAttribute('matchedRoute', $matchedRoute));
        } catch (RouteNotFoundException $e) {
            return new JsonResponse(['message' => 'Route not found'], 404);
        } catch (MethodNotAllowedException $e) {
            return new JsonResponse(['message' => 'Method Not Allowed'], 405, ['Allow' => join(',', $e->getAllowedMethods())]);
        } catch (\Throwable $e) {
            return new JsonResponse(['message' => $e->getMessage()], 500);
        }
    }
}