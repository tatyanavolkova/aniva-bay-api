<?php declare(strict_types=1);

namespace AnivaBay\Api\Domain\Service\Exception;

class NotFoundException extends ServiceException
{
}
