<?php declare(strict_types=1);
namespace AnivaBay\Api\Domain\Service;

use AnivaBay\Api\Domain\Entity\Article;
use AnivaBay\Api\Domain\Repository\Exception\NotFoundException as RepositoryNotFoundException;
use AnivaBay\Api\Domain\Repository\ArticlesRepository;
use AnivaBay\Api\Domain\Service\Exception\NotFoundException;
use JsonSchema;
use AnivaBay\Api\Infrastructure\Config;

class ArticlesService
{
    /**
     * @var ArticlesRepository
     */
    private $repository;

    /**
     * @var Config
     */
    private $config;

    /**
     * ArticlesService constructor.
     * @param ArticlesRepository $repository
     * @param Config $config
     */
    public function __construct(ArticlesRepository $repository, Config $config)
    {
        $this->repository = $repository;
        $this->config = $config;
    }

    /**
     * @return array
     * @throws NotFoundException
     */
    public function search(): array
    {
        try{
            $data = $this->repository->search();
            return $data;
        } catch (RepositoryNotFoundException $e) {
            throw new NotFoundException();
        }
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundException
     */
    public function searchOne($id): Article
    {
        try {
            return $this->repository->searchOne($id);
        } catch (RepositoryNotFoundException $e) {
            throw new NotFoundException();
        }
    }

    /**
     * @param $article
     * @return mixed
     */
    public function add($article)
    {
        return $this->repository->add($article);
    }

    /**
     * @param array $article
     * @return mixed
     */
    public function delete(int $id)
    {
        return $this->repository->delete($id);
    }
}

