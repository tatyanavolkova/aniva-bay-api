<?php
namespace AnivaBay\Api\Domain\Entity;


class Article implements \JsonSerializable
{
    /**
     * @var ?int
     */
    private $id;

    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $body;

    /**
     * @var \DateTime
     */
    private $added;

    /**
     * @var ?int
     */
    private $rate;

    /**
     * @var ?string
     */
    private $image;

    /**
     * @var ?string
     */
    private $category;

    /**
     * Article constructor.
     * @param ?int $id
     * @param string $title
     * @param string $description
     * @param string $body
     * @param \DateTime $added
     * @param ?int $rate
     * @param ?string $image
     */
    public function __construct(?int $id, string $title, string $description, string $body, \DateTime $added, ?int $rate, ?string $image, int $category)
    {
        if($id)
        {
            $this->id = $id;
        }
        $this->title = $title;
        $this->description = $description;
        $this->body = $body;
        $this->added = $added;
        $this->rate = $rate;
        $this->image = $image;
        $this->category = $category;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody(string $body): void
    {
        $this->body = $body;
    }

    /**
     * @return \DateTime
     */
    public function getAdded(): \DateTime
    {
        return $this->added;
    }

    /**
     * @param \DateTime $added
     */
    public function setAdded(\DateTime $added): void
    {
        $this->added = $added;
    }

    /**
     * @return int
     */
    public function getRate(): ?int
    {
        return $this->rate;
    }

    /**
     * @param int $rate
     */
    public function setRate(int $rate): void
    {
        $this->rate = $rate;
    }

    /**
     * @return mixed
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return int
     */
    public function getCategory(): int
    {
        return $this->category;
    }

    /**
     * @param int $category
     */
    public function setCategory(int $category): void
    {
        $this->category = $category;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'body' => $this->body,
            'added' => $this->added->format(\DateTime::RFC3339_EXTENDED),
            'rate' => $this->rate,
            'image' => $this->image,
            'category' => $this->category
        ];
    }


}