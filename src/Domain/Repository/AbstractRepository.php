<?php declare(strict_types=1);
namespace AnivaBay\Api\Domain\Repository;

use MongoDB\Collection;
use MongoDB\Database;
use MongoDB\Operation\FindOneAndReplace;
use MongoDB\Operation\FindOneAndUpdate;

abstract class AbstractRepository
{
    /**
     * @var Database
     */
    protected $db;

    /**
     * AbstractRepository constructor.
     * @param Database $db
     */
    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    /**
     * @return Collection
     */
    protected function getCollection(): Collection
    {
        return $this->db->selectCollection($this->getCollectionName());
    }

    /**
     *
     */
    public function validate()
    {

    }

    /**
     * @return int
     */
    public function generateId(): int
    {
        $result = $this->db->selectCollection('sequences')->findOneAndUpdate(
            ['_id' => $this->getCollectionName()],
            ['$inc' => ['counter' => 1]],
            [
                'returnDocument' => FindOneAndUpdate::RETURN_DOCUMENT_AFTER,
                'upsert' => true
            ]
        );
        return $result['counter'];
    }

    /**
     * @return string
     */

    abstract protected function getCollectionName(): string;
}
