<?php declare(strict_types=1);

namespace AnivaBay\Api\Domain\Repository\Exception;

class NotFoundException extends RepositoryException
{
}
