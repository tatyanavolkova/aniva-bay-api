<?php declare(strict_types=1);

namespace AnivaBay\Api\Domain\Repository;

use AnivaBay\Api\Domain\Entity\Article;
use MongoDB\BSON\UTCDateTime;
use MongoDB\Model\BSONDocument;
use AnivaBay\Api\Domain\Repository\Exception\NotFoundException;
use Zend\Diactoros\Response;

class ArticlesRepository extends AbstractRepository
{
    /**
     * @return array
     */
    public function search()
    {
        $data = $this->getCollection()->find()->toArray();

        $result = array_map(function (BSONDocument $item) {
            /** @var UTCDateTime $added */
            $added = $item['added'];
            return new Article(
                $item['_id'],
                $item['title'],
                $item['description'],
                $item['body'],
                $added->toDateTime(),
                $item['rate'],
                $item['image'],
                $item['category']
            );
        }, $data);

        return $result;
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundException
     */
    public function searchOne($id): Article
    {
        /** @var BSONDocument $article */
        $item = $this->getCollection()->findOne(['_id' => $id]);
        if (is_null($item)) {
            throw new NotFoundException();
        }
        return new Article(
            $item['_id'],
            $item['title'],
            $item['description'],
            $item['body'],
            $item['added']->toDateTime(),
            $item['rate'],
            $item['image'],
            $item['category']
        );
        //return iterator_to_array($article);
    }

    /**
     * @param $article
     * @return mixed
     */
    public function add(Article $article)
    {
        $data['_id'] = $this->generateId();
        $data['title'] = $article->getTitle();
        $data['description'] = $article->getDescription();
        $data['body'] = $article->getBody();
        $data['added'] = new \MongoDB\BSON\UTCDateTime($article->getAdded());
        $data['rate'] = $article->getRate();
        $data['image'] = $article->getImage();
        $data['category'] = $article->getCategory();

        $result = $this->getCollection()->insertOne($data);
        return $result->getInsertedId();
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        $result = $this->getCollection()->deleteOne(['_id' => $id]);
        return $result->getDeletedCount() > 0;
    }

    /**
     * @return string
     */
    protected function getCollectionName(): string
    {
        return 'Articles';
    }
}
