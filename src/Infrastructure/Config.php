<?php declare(strict_types=1);
namespace AnivaBay\Api\Infrastructure;

class Config
{
    /**
     * @var array
     */
    private $config;

    /**
     * Config constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $path
     * @param null $default
     * @param string $delimiter
     * @return array|mixed|null
     */
    public function get(string $path, $default = null, string $delimiter = '.')
    {
        $pathParts = explode($delimiter, $path);
        $current = &$this->config;
        foreach ($pathParts as $key) {
            if (!array_key_exists($key, $current)) {
                return $default;
            }
            $current = &$current[$key];
        }
        return $current;
    }
}
