<?php
namespace AnivaBay\Api\Helpers;

class ArrayHelper
{
    /**
     * @param array $arr
     * @return array|object
     */
    public static function arrayToObject(array $arr) {
        $flat = array_keys($arr) === range(0, count($arr) - 1);;
        $out = $flat ? [] : (object)[];

        foreach ($arr as $key => $value) {
            $value = is_array($value) ? ArrayHelper::arrayToObject($value) : $value;

            if ($flat) {
                $out[] = $value;
            } else {
                $out->{$key} = $value;
            }
        }
        return $out;
    }
}