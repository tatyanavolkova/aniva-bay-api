<?php declare(strict_types=1);
namespace AnivaBay\Api\Controller;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

interface ControllerInterface
{
    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function call(ServerRequestInterface $request): ResponseInterface;
}
