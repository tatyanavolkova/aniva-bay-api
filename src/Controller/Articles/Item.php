<?php declare(strict_types=1);

namespace AnivaBay\Api\Controller\Articles;

use MongoDB\Client;
use MongoDB\Database;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use AnivaBay\Api\Controller\ControllerInterface;
use AnivaBay\Api\Domain\Repository\ArticlesRepository;
use AnivaBay\Api\Domain\Service\Exception\NotFoundException;
use AnivaBay\Api\Domain\Service\ArticlesService;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\JsonResponse;

class Item implements ControllerInterface
{
    /**
     * @var ArticlesService
     */
    private $service;

    /**
     * Item constructor.
     * @param ArticlesService $service
     */
    public function __construct(ArticlesService $service)
    {
        $this->service = $service;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function call(ServerRequestInterface $request): ResponseInterface
    {
        $id = (int)$request->getAttribute('matchedRoute')->getArgs()['id'];
        try {
            $article =  $this->service->searchOne($id);
        } catch (NotFoundException $e) {
            return new JsonResponse('NotFound', 404);
        }
        return new JsonResponse($article);
    }
}
