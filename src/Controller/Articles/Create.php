<?php declare(strict_types=1);

namespace AnivaBay\Api\Controller\Articles;

use AnivaBay\Api\Domain\Entity\Article;
use MongoDB\BSON\UTCDateTime;
use MongoDB\Client;
use MongoDB\Database;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use AnivaBay\Api\Controller\ControllerInterface;
use AnivaBay\Api\Domain\Repository\ArticlesRepository;
use AnivaBay\Api\Domain\Service\ArticlesService;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\JsonResponse;

class Create implements ControllerInterface
{
    /**
     * @var ArticlesService
     */
    private $service;

    /**
     * Create constructor.
     * @param ArticlesService $service
     */
    public function __construct(ArticlesService $service)
    {
        $this->service = $service;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function call(ServerRequestInterface $request): ResponseInterface
    {
        $body = $request->getParsedBody();
        $article = new Article(null, $body->title, $body->description, $body->body, new \DateTime(), $body->rate, $body->image);

        if($this->service->add($article)) {
            return new EmptyResponse();
        }
    }
}
