<?php declare(strict_types=1);

namespace AnivaBay\Api\Controller\Articles;

use MongoDB\Client;
use MongoDB\Database;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use AnivaBay\Api\Controller\ControllerInterface;
use AnivaBay\Api\Domain\Repository\ArticlesRepository;
use AnivaBay\Api\Domain\Service\ArticlesService;
use Zend\Diactoros\Response\EmptyResponse;
use Zend\Diactoros\Response\JsonResponse;

class Delete implements ControllerInterface
{
    /**
     * @var ArticlesService
     */
    private $service;

    /**
     * Delete constructor.
     * @param ArticlesService $service
     */
    public function __construct(ArticlesService $service)
    {
        $this->service = $service;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function call(ServerRequestInterface $request): ResponseInterface
    {
        $id = (int)$request->getAttribute('matchedRoute')->getArgs()['id'];
        if($this->service->delete($id))
        {
            return new EmptyResponse();
        }
    }
}
