<?php declare(strict_types=1);

return [
    'mongo' => [
        'uri' => 'mongodb://mongo',
    ],

];
