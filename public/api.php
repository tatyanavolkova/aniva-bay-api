<?php declare(strict_types=1);
namespace AnivaBay\Api;

use Acelot\MiddlewareDispatcher\MiddlewareDispatcher;
use function Acelot\Resolver\factory;
use Acelot\Resolver\Resolver;
use function Acelot\Resolver\value;
use AnivaBay\Api\Http\Middleware\CorsHandler;
use Psr\Log\LoggerInterface;
use AnivaBay\Api\Http\Middleware\JsonParser;
use AnivaBay\Api\Http\Middleware\UnexpectedErrorHandler;
use AnivaBay\Api\Http\Middleware\Validator;
use MongoDB\Client;
use MongoDB\Database;
use AnivaBay\Api\Http\Middleware\Controller;
use AnivaBay\Api\Http\Middleware\RouteMatcher;
use AnivaBay\Api\Infrastructure\Config;
use Zend\Diactoros\Response\SapiEmitter;
use Zend\Diactoros\ServerRequestFactory;
use Monolog\Logger;
use Monolog\Handler\ErrorLogHandler;
use Monolog\Handler\StreamHandler;

require '../vendor/autoload.php';
$config = require '../config/default.php';
$config['api'] = yaml_parse_file('../config/api.yml');

$definitions = [
    Config::class => value(new Config($config)),
    Database::class => factory(function (Config $config) {
        $client = new Client($config->get('mongo.uri'));
        return new Database($client->getManager(), 'aniva-bay');
    }),
    LoggerInterface::class => factory(function () {
        $logger = new Logger('aniva-bay-api');
        $logger->pushHandler(new ErrorLogHandler(ErrorLogHandler::SAPI));
        return $logger;
    })
];
$resolver = new Resolver($definitions);

$dispatcher = new MiddlewareDispatcher($resolver, [
    UnexpectedErrorHandler::class,
    CorsHandler::class,
    RouteMatcher::class,
    JsonParser::class,
    Validator::class,
    Controller::class
]);

$request = ServerRequestFactory::fromGlobals();

$response = $dispatcher->handle($request);

$emitter = new SapiEmitter();
$emitter->emit($response);
